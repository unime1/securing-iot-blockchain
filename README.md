# Securing IoT Blockchain

## 1. Configurazione ambiente di esecuzione

### 1.1 Requisiti del progetto

1. Python v3.9
2. Python v3.9 pip
3. Python v3.9 venv
4. Docker

### 1.2 Python Virtual Environment

Il virtual environment sib-venv è stato creato con il comando:
`python3 -m venv sib-venv`

E' stato creato lo script start_virtual_environment.sh per accedere al Virtual Environment con più facilità, tramite il comando:
`source ./start_virtual_environment.sh`

A questo punto è possibile procedere con l'installazione dei moduli necessari al progetto.

### 1.3 Creazione del file requirements.txt

---

### 1.3 Creazione dei files requirements.txt

Per ogni cartella, rappresentante un nodo indipendente del sistema, creiamo il file delle dipendenze necessarie per eseguire il server all'interno di un ambiente isolato. Supponendo di trovarci all'interno della cartella securing-iot-blockchain, eseguiamo i seguenti comandi.

Certification authority:
`cd ./ca_server && pipreqs ./`

IoT Device:
`cd ./iot_device && pipreqs ./`

Trusted gateway:
`cd ./trusted_gateway_server && pipreqs ./`

---

Installando il modulo pipreqs con il comando
`python3 -m pip install pipreqs`

è possibile esaminare le dipendenze del progetto. Il file requirements.txt verrà creato con il comando:
`pipreqs ./securing-iot-blockchain`

Errori:

- Rilevazione del modulo wincertstore. Su Linux causa un errore e ferma l'installazione.

Fonte:

- https://stackoverflow.com/questions/31684375/automatically-create-requirements-txt

### 1.4 Installazione delle dipendenze

Una volta creato il file requirements.txt possiamo installare le dipendenze nel virtual environment con il comando:
`python3 -m pip install -r requirements.txt`

Fonte:

- https://pip.pypa.io/en/stable/user_guide/

## CA Node

Completati gli step precedenti per l'esecuzione dell'ambiente di esecuzione, spostarsi nella cartella ca_server ed eseguire il comando:
`python3 server.py`

In alternativa, il server può essere eseguito come di seguito:

1. Esportare la variabile di ambiente con: `export FLASK_APP=server.py`
2. Eseguire il comando `flask run --host 127.0.0.1 --port 5010` per avviare il server.

## Trusted Gateway Node

Completati gli step precedenti per l'esecuzione dell'ambiente di esecuzione, spostarsi nella cartella trusted-gateway_server ed eseguire il comando:
`python3 app.py`

In alternativa, il server può essere eseguito come di seguito:

1. Esportare la variabile di ambiente con: `export FLASK_APP=app.py`
2. Eseguire il comando `flask run --host 127.0.0.1 --port 5011` per avviare il server.

Errori:

- Installazione del modulo web3: https://stackoverflow.com/questions/22571848/debugging-the-error-gcc-error-x86-64-linux-gnu-gcc-no-such-file-or-directory

---

## Infrastruttura locale

- Ricerca dispositivi in rete: https://itsfoss.com/how-to-find-what-devices-are-connected-to-network-in-ubuntu/

## Copia dei Server sulle Raspberry

- IoT: `scp -r iot_server/ pi@IOT_IP:~`
- Certification Authority: `scp -P 2222 -r ./ca_server/ user@CA_IP:~`
- Trusted gateway: `scp -r trusted-gateway_server/ pi@TG_IP:~`

---

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:e17ceba699ee2fec5a456efbf8a092c8?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:e17ceba699ee2fec5a456efbf8a092c8?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:e17ceba699ee2fec5a456efbf8a092c8?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/unime1/securing-iot-blockchain.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:e17ceba699ee2fec5a456efbf8a092c8?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:e17ceba699ee2fec5a456efbf8a092c8?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:e17ceba699ee2fec5a456efbf8a092c8?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:e17ceba699ee2fec5a456efbf8a092c8?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:e17ceba699ee2fec5a456efbf8a092c8?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:e17ceba699ee2fec5a456efbf8a092c8?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:e17ceba699ee2fec5a456efbf8a092c8?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:e17ceba699ee2fec5a456efbf8a092c8?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:e17ceba699ee2fec5a456efbf8a092c8?https://docs.gitlab.com/ee/user/clusters/agent/)

---

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://gitlab.com/-/experiment/new_project_readme_content:e17ceba699ee2fec5a456efbf8a092c8?https://www.makeareadme.com/) for this template.
