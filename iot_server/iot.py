import base64
import os
import time
import shutil
import requests
import ssl
import math
from flask import Flask, flash, request, redirect, url_for, send_from_directory, render_template
from cryptography import x509
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.x509.oid import NameOID
from werkzeug.utils import secure_filename
from datetime import datetime
from getmac import get_mac_address
from cryptography.hazmat.primitives.serialization import load_pem_private_key
# Exception
from requests import exceptions

TEMPORARY_FOLDER = 'tmp'
KEY_FOLDER = 'keys'
ALLOWED_EXTENSIONS = {'pem'}
CA_IP = "192.168.1.134"

app = Flask(__name__)
app.config['TEMP'] = TEMPORARY_FOLDER
app.config['KEY_FOLDER'] = KEY_FOLDER

initialized = False
device_num = 1

"""
start_time = time.clock() ### seconds
"""

''''
==== TECH SIGN FOLDER ====
- Technical pass its encrypted private key in a temporary folder
'''


@app.route("/tech-key-upload", methods=['GET', 'POST'])
def tech_key_upload():

    # Render form to insert Tech Encrypted Key
    if request.method == 'GET':
        return render_template('tech-key-upload.html')

    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)

        file = request.files['file']
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)

        # Tech Upload the key
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join('./' + app.config['TEMP'], filename))

            print("[---IoT---]: Tech private key uploaded.")

            return "ENCRYPTED TECH KEY UPLOADED! <br>" + \
                "Return to <a href='/'>Home Page</a>"


'''
==== GENERATE IOT CSR ====
- Extract info to create a CSR for the IoT
- Sign the CSR with Technical private key
'''


@app.route("/generate-csr", methods=['GET', 'POST'])
def generate_csr():
    global device_num

    # START SPEED TEST - TEST NUMBER
    with open("./test-iot.csv", "a") as f:
        f.write(str(device_num))
        f.write(",")

    # Getting Tech Key from the tmp folder
    # !!! SECURE THIS STEP !!!
    with open('./' + app.config['TEMP'] + '/tech-private.pem', "rb") as key_f:
        tech_key = load_pem_private_key(key_f.read(), password=None)

    # SPEED TEST - IOT_KEY_TIME
    start_time = datetime.now()
    print(start_time)

    # Generate IoT Key
    if os.path.isfile('./' + app.config['KEY_FOLDER'] + '/iot-dev' + str(device_num) + '.key'):
        pass
    else:
        generate_iot_key(device_num)

    # Getting IoT Public Key
    with open('./' + app.config['KEY_FOLDER'] + '/iot-dev' + str(device_num) + '.key', "rb") as key_f:
        iot_key = load_pem_private_key(key_f.read(), password=None)

    pub_k_obj = iot_key.public_key()
    iot_pub_key = pub_k_obj.public_bytes(
        serialization.Encoding.PEM, serialization.PublicFormat.SubjectPublicKeyInfo)

    print("[---IoT---]: IoT Key-pair created!")

    # SPEED TEST - Writing..
    end_time = datetime.now()
    print(end_time)
    storeTime(start_time, end_time, ",")

    # SPEED TEST - CSR_BUILD
    start_time = datetime.now()
    # Device name
    user_id = "iot.dev." + str(device_num)

    # MAC Address
    print("[---IoT---]: Get MAC Address.")
    mac_address = get_mac_address(interface="enp2s0")

    csr = x509.CertificateSigningRequestBuilder().subject_name(x509.Name([
        # u before string: https://stackoverflow.com/questions/2081640/what-exactly-do-u-and-r-string-flags-do-and-what-are-raw-string-literals
        x509.NameAttribute(NameOID.COUNTRY_NAME, u"IT"),
        x509.NameAttribute(NameOID.STATE_OR_PROVINCE_NAME, u"Italy"),
        x509.NameAttribute(NameOID.LOCALITY_NAME, u"Messina"),
        x509.NameAttribute(NameOID.ORGANIZATION_NAME, u"UniMe"),
        x509.NameAttribute(NameOID.ORGANIZATIONAL_UNIT_NAME,
                           u"IoT Device " + str(device_num)),
        x509.NameAttribute(NameOID.COMMON_NAME, mac_address),
        x509.NameAttribute(NameOID.USER_ID, user_id),
        x509.NameAttribute(NameOID.DOMAIN_COMPONENT,
                           iot_pub_key.decode("utf-8")),
    ])).sign(tech_key, hashes.SHA256())

    device_num = device_num + 1

    with open("./device-info/iot-csr.pem", "wb") as f:
        f.write(csr.public_bytes(serialization.Encoding.PEM))

    print("[---IoT---]: IoT CSR released.")

    # SPEED TEST - Writing..
    end_time = datetime.now()
    storeTime(start_time, end_time, "\n")

    return "CSR Created! <a href='/'> Return to Home Page</a>"


'''
==== SEND IOT CSR TO CA ====
- Read CSR file
- Send CSR to CA Server to sign and release certificate
'''


@app.route("/send-csr", methods=['GET', 'POST'])
def send_csr():

    with open("./device-info/iot-csr.pem", "rb") as f:
        csr = x509.load_pem_x509_csr(f.read())

    # Send CSR to CA Server
    files = {'csr': csr.public_bytes(serialization.Encoding.PEM)}
    try:
        req = requests.post('https://' + CA_IP + ':5010/sign-csr',
                            files=files, verify=False)
        print("[---IoT---]: IoT CSR sent.")
    except requests.exceptions.ConnectionError:
        return "CA Server is not Active"
    else:
        return "CSR Sent"


'''
==== INDEX PAGE ====
- Upload Private Tech Key
- Create CSR for device
- Send CSR to CA Server to sign and release certificate
'''


@app.route("/")
def index():
    global initialized
    global device_num
    if initialized == False:
        initialized = True
        main()
    return render_template("index.html", device_num=device_num)


def generate_iot_key(device_num):

    if os.path.isdir('./keys'):
        # os.getcwd: Get current working directory
        os.chdir(os.getcwd() + "/" + "keys")

    os.system(
        "openssl genpkey -out iot-dev" + str(device_num) + ".key -algorithm RSA -pkeyopt rsa_keygen_bits:2048")

    os.chdir('../')
    return 0


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def storeTime(start_time, end_time, ch):
    delta = end_time - start_time
    milliseconds = math.ceil(delta.total_seconds() * 1000)
    with open("./test-iot.csv", "a") as f:
        f.write(str(milliseconds))
        f.write(ch)

# Initial configuration


def main():

    # If folder keys exist, remove it
    if os.path.isdir("./keys"):
        print("[---IoT---]: Deleting folder keys..")
        shutil.rmtree("keys")

    path = os.getcwd()  # Get current working directory
    try:
        os.mkdir("keys")  # Try to create a new directory
    except(OSError):
        print("[---IoT---]: Folder \'keys\' already exists!")
    else:
        print("[---IoT---]: Folder \'keys\' created!")
    return 0


# #### START SERVER #### #
if __name__ == "__main__":
    app.run(debug=False, host='0.0.0.0', port=5012)

'''
==== File Download ====
- https://flask.palletsprojects.com/en/2.0.x/patterns/fileuploads/?highlight=download

==== Startup Function Execution ===
- https://stackoverflow.com/questions/27465533/run-code-after-flask-application-has-started
'''
'''
# resp = requests.get('https://127.0.0.1:5010/', verify=True,
    #                    cert=['./ca_certificate/root-ca.crt'])
'''

"""
# ### COMMON ERRORS
    - Error process a request to a web server protect with SSL:
    $ Solution: https://stackoverflow.com/questions/10667960/python-requests-throwing-sslerror
"""
