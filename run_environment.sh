#!/bin/bash

gnome-terminal --title="Blockchain" -- docker container start blockchain -i
gnome-terminal --title="IPFS Node 1" -- docker container start ipfs-node-1 -i
gnome-terminal --title="IPFS Node 2" -- docker container start ipfs-node-2 -i
#gnome-terminal --title="CA Server" -- docker container start ca-server-1 -i 
