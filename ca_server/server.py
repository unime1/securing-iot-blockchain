import os
import requests
import math
from datetime import datetime
from flask import Flask, flash, request, redirect, url_for, send_from_directory, render_template
from werkzeug.utils import secure_filename
from cryptography import x509
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.x509.oid import NameOID
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.serialization import load_pem_private_key, load_pem_public_key

IOT_DATA_FOLDER = 'iot-device-data'
ROOT_PATH = './'
CRT_FOLDER = './etc/ssl/certs'
CA_CERT = './etc/ssl/ca/'
CA_CERTIFICATE = './etc/ssl/ca/root-ca.crt'
TECH_CERTIFICATE = './etc/ssl/ca/out/tech-identity.crt'
CA_KEY = './etc/ssl/ca/root-ca/private/root-ca.key'
ALLOWED_EXTENSIONS = {'csr', 'pem', 'crt', 'enc'}
TG_IP = "192.168.1.121"

initialized = False

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = IOT_DATA_FOLDER
app.config['CERT_FOLDER'] = CRT_FOLDER
app.config['CA_CERT'] = CA_CERT


'''
==== SIGN IOT CERTIFICATE ====
1. Get the certificate from POST request
2. Create a new Certificate containing csr info and CA info
3. Sign Certificate and store it into ssl/certs folder
'''


@app.route('/sign-csr', methods=['GET', 'POST'])
def validate_request(iot_data=None):

    path = app.config["UPLOAD_FOLDER"]

    if request.method == "GET":
        return "This endpoint wait for a POST request with CSR as file"

    if request.method == "POST":

        # SPEED TEST - CRT_BUILD
        start_time = datetime.now()

        # CSR Object Creation
        csr_data = request.files['csr'].stream.read()
        # file.save(f"./iot-device-csr/{secure_filename(file.filename)}")

        csr = x509.load_pem_x509_csr(csr_data)
        print("[---CA LOGS ---]: Load CSR.")

        iot_pub = load_pem_public_key(
            csr.subject.get_attributes_for_oid(
                NameOID.DOMAIN_COMPONENT).pop().value.encode()
        )

        iot_name = csr.subject.get_attributes_for_oid(
            NameOID.ORGANIZATIONAL_UNIT_NAME).pop().value

        iot_certificate_name = iot_name.replace(" ", "_")

        # Verify with Public TECH
        verified = True
        if verified:

            # Load CA Certificate
            ca_certificate = x509.load_pem_x509_certificate(
                download_pem_ca().encode())

            # Create Certificate Builder Assistant
            builder = x509.CertificateBuilder()

            # Write Subject Info
            builder = builder.subject_name(x509.Name([
                csr.subject.get_attributes_for_oid(
                    NameOID.COUNTRY_NAME).pop(),
                csr.subject.get_attributes_for_oid(
                    NameOID.STATE_OR_PROVINCE_NAME).pop(),
                csr.subject.get_attributes_for_oid(
                    NameOID.LOCALITY_NAME).pop(),
                csr.subject.get_attributes_for_oid(
                    NameOID.ORGANIZATION_NAME).pop(),
                csr.subject.get_attributes_for_oid(
                    NameOID.ORGANIZATIONAL_UNIT_NAME).pop(),
                csr.subject.get_attributes_for_oid(
                    NameOID.STATE_OR_PROVINCE_NAME).pop(),
                csr.subject.get_attributes_for_oid(
                    NameOID.COMMON_NAME).pop(),
                csr.subject.get_attributes_for_oid(
                    NameOID.USER_ID).pop(),
            ]))

            # Write Issuer (CA) Info
            builder = builder.issuer_name(
                ca_certificate.issuer
            )

            # Start and Expiration Dates
            builder = builder.not_valid_before(
                datetime.fromisoformat('2022-01-01'))
            builder = builder.not_valid_after(
                datetime.fromisoformat('2022-12-31'))

            # Generate a serial number for the certificate
            builder = builder.serial_number(x509.random_serial_number())

            # Put the public key of the CSR inside the certificate
            builder = builder.public_key(iot_pub)

            # Get the CA private key (questa operazione deve essere eseguita
            # da un utente che ha i permessi per accedere alla cartella private)
            # (risolvere il problema dei permessi)
            with open(CA_KEY, "rb") as key_f:
                ca_private = load_pem_private_key(
                    key_f.read(), password=b"caserver2022")

            certificate = builder.sign(
                private_key=ca_private, algorithm=hashes.SHA256(),
            )

            # Verify instance
            print("[---CA LOGS ---]: Ok, its a x509 Certificate!" if isinstance(
                certificate, x509.Certificate) else "ATTENTION! This is not a x509 certificate")

            # Store IoT Certificate to certs folder
            with open(app.config['CERT_FOLDER'] + '/' + iot_certificate_name + '.pem', "wb") as f:
                f.write(certificate.public_bytes(serialization.Encoding.PEM))

            # START SPEED TEST - TEST NUMBER
            with open("./test-ca.csv", "a") as f:
                f.write(str(iot_name.split(' ')[-1]))
                f.write(",")
            # SPEED TEST - Writing
            storeTime(start_time, datetime.now(), "\n")

            print("[---CA LOGS ---]: Certificate Released!")
        return "<h3> CRT Released! </h3> <br> <a href='/tg-pub-dashboard'> Send IoT certificate to Trusted Gateway </a>"
    '''
    return "Certificate stored!"
    # return render_template('csr-list.html', csr_list=file)
    # return send_from_directory(app.config["UPLOAD_FOLDER"], name)
    '''


"""
==== PENDING CERTIFICATE ====
Certificate released but not uploaded to Trusted Gateway
1. Get a list of certificates in the folder ./ssl/certs
2. Get a list of certificates uploaded in the Trusted Gateway
3. Create two sets and do difference between them to obtain the certificate
4. Return a table with buttons to push the missing certificate
"""


@app.route('/tg-pub-dashboard', methods=['GET', 'POST'])
def publish_certificate():
    if request.method == 'GET':
        certs_path = app.config["CERT_FOLDER"]

        # Get list of certificates in the folder
        ca_certs = set(os.listdir(certs_path))

        # Get list of certificate in the trusted gateway
        req = requests.get('http://' + TG_IP + ':5011/cert-list')
        res = req.json()
        tg_certs = set(res['Certificate'])

        # List of missing certificate
        miss_certs = list(ca_certs.difference(tg_certs))
        return render_template('pub-dashboard.html', miss_certs=miss_certs)

    if request.method == 'POST':
        # check if the post request has the filename in form
        if 'crt_filename' not in request.form:
            flash('No certificate filename')
            return redirect(request.url)

        crt_f = open(app.config["CERT_FOLDER"] + '/' +
                     request.form['crt_filename'], "rb")

        if crt_f and allowed_file(crt_f.name):

            filename = secure_filename(crt_f.name)
            print("Current folder:", os.getcwd())
            files = {'certificate': crt_f}
            req = requests.post(
                'http://' + TG_IP + ':5011/cert-saver', files=files)

            return req.text
            #file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            # return redirect(url_for('validate_request', csr_name=filename))

        # Endpoint per l'accesso del tecnico
        # Se il tecnico non ha un certificato, lo reinderiziamo in una pagina per crearlo
        # Altrimenti lo reinderiziamo in una pagina per effettuare la richiesta del certificato per l'IoT


"""
========= TEST TRUSTED GATEWAY CONNECTION ============
1. Test della connessione al trusted gateway

% Print Dictionary Keys
| - https://stackoverflow.com/questions/5904969/how-to-print-a-dictionarys-key
| for key, value in app.config.items():
|    print(key, value)
"""


@app.route("/test-tg-conn")
def test_tg_conn():
    req = requests.get('http://' + TG_IP + ':5011')

    request_info = {
        "Status": str(req.status_code),
        "Headers": str(req.headers['content-type']),
        "Encoding": str(req.encoding),
        "Response": str(req.text)
    }

    return request_info


'''
============= CERTIFICATE DOWNLOAD ===================
| Endpoint where client can download the PEM         |
| certificate of the CA and the TECH                 |
'''


@app.route("/ca-cert", methods=['GET'])
def download_pem_ca():
    with open(CA_CERTIFICATE, "r") as ca_crt:
        ca_crt_data = ca_crt.read()
    return ca_crt_data


'''
======== TECHNICIAN CERTIFICATE DOWNLOAD =============
| Endpoint where client can download the PEM         |
| certificate of the CA and the TECH                 |
'''


@app.route("/tech-cert", methods=['GET'])
def download_pem_tech():
    with open(TECH_CERTIFICATE, "r") as tech_crt:
        tech_crt_data = tech_crt.read()
    return tech_crt_data


'''
================= INDEX PAGE =========================
'''


@app.route("/")
def index():
    global initialized
    if initialized == False:
        initialized = True
        main()
    # vedi Certificati salvati in Mozilla Firefox
    ca_certificate = "data:," + download_pem_ca()
    tech_certificate = "data:," + download_pem_ca()
    return render_template("index.html", ca_certificate=ca_certificate, tech_certificate=tech_certificate)


'''
================== SUPPORT FUNCTIONS =================
'''


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def storeTime(start_time, end_time, ch):
    delta = end_time - start_time
    milliseconds = math.ceil(delta.total_seconds() * 1000)
    with open("./test-ca.csv", "a") as f:
        f.write(str(milliseconds))
        f.write(ch)


@app.route('/uploads/<path:filename>', methods=['GET', 'POST'])
def download(filename):
    uploads = os.path.join('./', app.config['UPLOAD_FOLDER'])
    return send_from_directory(path=ROOT_PATH, directory=uploads, filename=filename)


'''
=================== STARTUP SETUP =====================
| - Signing Technician certificate                    |
'''


def main():

    # Load CA certificate
    ca_certificate = x509.load_pem_x509_certificate(download_pem_ca().encode())
    with open(CA_KEY, "rb") as key_f:
        ca_private = load_pem_private_key(
            key_f.read(), password=b"caserver2022")

    # Technician Admin Key Creation with ECC
    print("[---CA LOGS ---]: Technician Key-pair creation")
    private_key = ec.generate_private_key(ec.SECP384R1())

    # Extract Public Key
    public_key = private_key.public_key()

    # Creating Certificate for Technician
    print("[---CA LOGS ---]: Building Technician certificate..")
    builder = x509.CertificateBuilder()
    builder = builder.subject_name(x509.Name([
        x509.NameAttribute(NameOID.COMMON_NAME, u'Technician'),
    ]))
    builder = builder.issuer_name(
        ca_certificate.subject
    )
    builder = builder.not_valid_before(datetime.fromisoformat('2022-01-01'))
    builder = builder.not_valid_after(datetime.fromisoformat('2022-12-31'))
    builder = builder.serial_number(x509.random_serial_number())
    builder = builder.public_key(public_key)

    print("[---CA LOGS ---]: Sign Technician certificate")
    tech_certificate = builder.sign(
        private_key=private_key, algorithm=hashes.SHA256(),
    )

    with open("./etc/ssl/ca/out/tech-identity.crt", "wb") as tech_crt:
        tech_crt.write(tech_certificate.public_bytes(
            serialization.Encoding.PEM))

    print("[---CA LOGS ---]: Technician Certificate stored!")

    return 0


# #### START SERVER #### #
if __name__ == "__main__":
    app.run(debug=False, host='0.0.0.0', port=5010,
            ssl_context=(CA_CERTIFICATE, CA_KEY))

"""
==== HTTPS Server =====
- https://blog.miguelgrinberg.com/post/running-your-flask-application-over-https
- ssl_context parameter: https://stackoverflow.com/questions/29458548/can-you-add-https-functionality-to-a-python-flask-web-server
- Test SSL: https://www.ssllabs.com/ssltest/index.html
=======================

# ### COMMON ERRORS
--------------------------------------------------------
- Error loading CSR:
    # NoneType Error: https://stackoverflow.com/questions/65007010/how-to-use-python-cryptography-to-load-a-der-certificate-without-backend-or-wha
$ Solution: python3 -m pip install --upgrade cryptography
---------------------------------------------------------

# ### TIPS
- # u before string: 
    https://stackoverflow.com/questions/2081640/what-exactly-do-u-and-r-string-flags-do-and-what-are-raw-string-literals
"""

'''
=== EXCEPTION HANDLING ===
Func: publish_certificate
- Prevent FileNotFoundError
- 
'''
