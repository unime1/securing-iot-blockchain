// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

pragma experimental ABIEncoderV2;

contract DevCert {
  constructor() public {
  }
  
  // This declares a new complex type which will
    // be used for variables later.
    // It will represent an IoT Device
    struct Device {
        address gatewayAddress; // Account address Trusted Gateway
        string deviceName; // the name of IoT Device
        string macAddress; // identificator of the device
        bool certified; // is the device certificate released or not expired
        //address technician; // the technician who request the certification
        string certCid; // certificate location in IPFS File System
        string timestamp; // last update information
    }

    address gatewayAddr; // the address of the trusted gateway node, who store certificate

    // This declares a state variable that
    // stores a `Device` struct for each possible string.
    mapping(string => Device) public devices;

    // A dynamically-sized array of `Device` structs.
    string[] public deviceList;

    /// Create a new ballot to choose one of `proposalNames`.
    //constructor() {
    //    tg_addr = msg.sender; // sender of the message (current call)
    //}

    function storeCert(address trusted_gateway) external {}

    // Set Device info method
    function addDevice(
        address gatewayAddress,
        string memory deviceName,
        string memory macAddress,
        bool certified,
        string memory certCid,
        string memory timestamp
    ) public {
        Device storage device = devices[deviceName]; // Assign the space in devices to a string, return a new editable Device Object

        device.gatewayAddress = gatewayAddress;
        device.deviceName = deviceName;
        device.macAddress = macAddress;
        device.certified = certified;
        device.certCid = certCid;
        device.timestamp = timestamp;

        deviceList.push(deviceName); // To remember the string assigned to a Struct into the devices mapping, store the string in this support array

        /*
        deviceList.push(
            Device({
                blockId: blockId,
                macAddress: macAddress,
                certified: certified,
                certCid: certCid,
                timestamp: timestamp
            })
        );
        */
    }

    function getDevices() public view returns (string[] memory) {
        return deviceList;
    }

    function readDevice(string memory deviceName)
        public
        view
        returns (
            string memory,
            address,
            string memory,
            bool,
            string memory,
            string memory
        )
    {
        return (
            devices[deviceName].deviceName,
            devices[deviceName].gatewayAddress,
            devices[deviceName].macAddress,
            devices[deviceName].certified,
            devices[deviceName].certCid,
            devices[deviceName].timestamp
        );
    }

    function dialup() public pure returns (string memory) {
        return "Hi! I'm the smart contract DevCert";
    }
}
