'''
=== Visual Studio Code ===
# _select-and-activate-an-environment
- Select Interpreter / Virtual Environment: https://code.visualstudio.com/docs/python/environments
- setup.py vs requirements.txt https://caremad.io/posts/2013/07/setup-vs-requirement/
'''
from setuptools import setup

setup()

#  Issue: ERROR: Files/directories not found in /tmp/pip-req-build-qhfxvln7/pip-egg-info
#  Solve with:
#  - pip install testresources
#  - pip install --upgrade setuptools
