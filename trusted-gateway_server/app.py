'''
==== TRUSTED GATEWAY SERVER ====
1. Retrieve Certificate (CRT) from CA Server
2. Send CRT to IPFS
3. Obtain CID from IPFS about CRT is stored
4. Write the CID in the Blockchain of Ethereum
'''


# ### Support imports
import os
import json
import math
from werkzeug.utils import secure_filename
# ### Server imports
from flask import Flask, flash, request, redirect, url_for, send_from_directory, render_template
# ### Client imports
import requests
# ### Certificate imports
from cryptography import x509
from cryptography.x509.oid import NameOID
from datetime import datetime
# ### Blockchain imports
from web3 import Web3, HTTPProvider
from werkzeug.wrappers import response

CERT_FOLDER = './iot-certs'
CONTRACT_ADDRESS = "0x0c9011334Dd4880094492D956E70c002816D9bF0"

app = Flask(__name__)
app.config['CERT_FOLDER'] = CERT_FOLDER
app.config['IPFS_IP'] = "192.168.149"
app.config['BLOCKCHAIN_IP'] = "192.168.149"

known_devices = []

"""
=========== LIST OF CERTIFICATE STORED IN THE NODE ===========
| - GET: Return a list of the filename into iot-certs folder |
"""


@app.route('/cert-list', methods=['GET'])
def cert_list():
    if request.method == 'GET':
        certs_names = {'Certificate': os.listdir('./iot-certs')}
        print("[---TG---]: Return a list of certificate stored in.")
        return certs_names


"""
=========== STORE OF CERTIFICATE STORED IN THE NODE ===========
| - POST: Store a new certificate in the iot-certs folder     |
"""


@app.route('/cert-saver', methods=['POST'])
def cert_saver():
    if request.method == 'GET':
        return "Do a POST to store certificate in the Trusted Gateway storage"

    if request.method == 'POST':
        print(request.files)
        file = request.files['certificate']
        file.save(f"./iot-certs/{secure_filename(file.filename)}")
        print("[---TG---]: Certificate uploaded.")
        return "Certificate Uploaded"


"""
================ IPFS UPLOADER =================
| 1. Get Certificate file from folder iot-certs
| 2. POST data through the API of IPFS
| 3. Retrieve CID
| 4. Store the CID in a db file (or real DB)
"""


@app.route('/ipfs-push-cert', methods=['GET', 'POST'])
def ipfs_push_cert():
    certs_path = app.config["CERT_FOLDER"]
    global known_devices

    if request.method == "GET":

        # Get list of certificates in the folder
        tg_certs = set(os.listdir(certs_path))

        # Read to the Database which certificate aren't added to IPFS
        print(tg_certs, known_devices)
        new_list = []
        for device in tg_certs:
            if device.split(".")[0] in known_devices:
                pass
            else:
                new_list.append(device)
        # List of missing certificate
        miss_certs = new_list

        return render_template('ipfs-pub-dashboard.html', miss_certs=miss_certs)

    if request.method == "POST":
        # Verify the name exists
        # check if the post request has the filename in form
        if 'crt_filename' not in request.form:
            flash('No certificate filename')
            return redirect(request.url)

        # SPEED TEST - IPFS_UPLOAD
        start_time = datetime.now()

        # Read the file
        files = {'file': open(
            './' + certs_path + '/' + request.form['crt_filename'], 'rb')}

        print("[---TG---]: Certificate selected")

        # Upload certificate in IPFS
        req = requests.post(
            "http://192.168.1.149:5001/api/v0/add", files=files)

        print("[---TG---]: IPFS add.. Done!")

        # Get response in Dict Python format.
        response_json = req.json()
        print(response_json['Name'])

        # START SPEED TEST - TEST NUMBER
        with open("./test-tg.csv", "a") as f:
            f.write(str(response_json['Name'].split("_")[-1].split(".")[0]))
            f.write(",")

        # SPEED TEST - Writing..
        storeTime(start_time, datetime.now(), ",")

        # SPEED TEST - IPFS_READ_NODE_2
        start_date = datetime.now()

        # Retrieve certificate from second IPFS Node
        params = (('arg', response_json['Hash']),)
        req2 = requests.post(
            "http://192.168.1.149:5002/api/v0/cat", params=params)
        if req2.status_code == 200:
            print("[---TG---]: Certificate from IPFS Node 2 taken")
        else:
            print("[---TG---]: Error in POST request to IPFS Node 2")

        # SPEED TEST - Writing..
        storeTime(start_time, datetime.now(), ",")

        # Append to DB file
        with open('ipfs_cid.db', 'a') as db_f:
            db_f.write(response_json['Name'] + '\t' +
                       response_json['Hash'] + '\n')
            db_f.close()

        print("[---TG---]: Database built")

        with open('./' + certs_path + '/' + request.form['crt_filename'], 'rb') as f:
            crt = x509.load_pem_x509_certificate(f.read())
            mac_address = crt.subject.get_attributes_for_oid(
                NameOID.COMMON_NAME).pop().value

        # SPEED TEST - BLOCKCHAIN_MEMO
        start_time = datetime.now()

        # Store CID in the blockchain
        deviceName = response_json['Name'].split('.')[0]
        data = {
            'deviceName': deviceName,
            'gatewayAddress': '',
            # MAC Address del certificato e' ricavabile dal certificato
            'macAddress': mac_address,
            'certified': True,
            'certCid': response_json['Hash'],
            'timestamp': datetime.now().strftime("%Y-%m-%d, %H:%M:%S")
        }

        print("[---TG---]: Data for Blockchain ready")

        # Send data to Blockchain
        known_devices = addDevice(data)

        print("[---TG---]: IoT registered in the Blockchain")

        # END SPEED TEST - Writing..
        storeTime(start_time, datetime.now(), "\n")

        print("Certificate stored on IPFS and CID saved into the Blockchain")

        return "Response from Blockchain: " + str(known_devices)


"""
==== BLOCKCHAIN RPC ====
1. Interface to Blockchain Ganache RPC
2. Store the CID into the blockchain
"""


# @app.route('/add-device-blockchain', methods=['GET', 'POST'])
# req2 = requests.post(
#    'http://127.0.0.1:5000/add-device-blockchain', json=data)
def addDevice(data):

    if request.method == "GET":
        return "Do a POST request to put the Certified Device Info into the blockchain"

    if request.method == "POST":
        web3 = blockchain_setup()

        if web3 is not None:

            DevCert = contract_selection(
                contract_name="DevCert", contract_addr=CONTRACT_ADDRESS, web3=web3)

            data['gatewayAddress'] = web3.eth.default_account

            DevCert.functions.addDevice(
                data['gatewayAddress'],
                data['deviceName'],
                data['macAddress'],
                data['certified'],
                data['certCid'],
                data['timestamp']
            ).transact()

            # Perform the transaction
            return DevCert.functions.getDevices().call()

        else:
            return "No web3 object returned! Check if the Blockchain is active. <br> <a href='/'s> Return to Home Page </a>"


def getDevices():
    if request.method == "GET":
        return "Do a POST request to put the Certified Device Info into the blockchain"

    if request.method == "POST":
        web3 = blockchain_setup()

        if web3 is not None:

            DevCert = contract_selection(
                contract_name="DevCert", contract_addr=CONTRACT_ADDRESS, web3=web3)

            DevCert.functions.listDevices().transact()

            # Perform the transaction
            return str(DevCert.functions.getDevices().call())

        else:
            return "No web3 object returned! Check if the Blockchain is active. <br> <a href='/'s> Return to Home Page </a>"


@app.route('/test-contract', methods=['GET'])
def greetings_bc():

    web3 = blockchain_setup()

    # Get the contract
    if web3 is not None:
        DevCert = contract_selection(
            contract_name="DevCert", contract_addr=CONTRACT_ADDRESS, web3=web3)
    else:
        return "No web3 object returned! Check if the Blockchain is active. <br> <a href='/'s> Return to Home Page </a>"
    # Test Contract Interaction
    sc_response = DevCert.functions.dialup().call()

    # At first call, it was an empty list
    # deviceList = DevCert.functions.getDevices().call()

    return sc_response


"""
-----------------BLOCKCHAIN SETUP FUNCTION------------------------------
"""


def blockchain_setup():
    # /// BLOCKCHAIN CONNECTION \\\
    web3 = Web3(HTTPProvider("http://192.168.1.149:7545"))
    if (web3.isConnected()):
        # this trusted gateway has the first account of the blockchain
        web3.eth.default_account = web3.eth.accounts[0]
        return web3

    return None


def contract_selection(web3, contract_name, contract_addr):
    # DevCert Contract ABI&Address
    with open('./contract/abi/' + contract_name + '.json', 'r') as c:
        c_info = json.load(c)
        c_abi = c_info["abi"]
        c_address = contract_addr

    contract = web3.eth.contract(address=c_address, abi=c_abi)

    return contract


def storeTime(start_time, end_time, ch):
    delta = end_time - start_time
    milliseconds = math.ceil(delta.total_seconds() * 1000)
    with open("./test-tg.csv", "a") as f:
        f.write(str(milliseconds))
        f.write(ch)


@app.route("/")
def index():
    return render_template('index.html')
    # "<h3> !!! Trusted Gateway Service !!! </h3>"


if __name__ == "__main__":
    app.run(debug=False, host='0.0.0.0', port=5011)

# cURL Request: send the file in form parameter of curl
# curl --location --request POST 'http://127.0.0.1:5000/cert-manager' \
# --form 'iot-cert=@test1.txt;type=text/plain'
# IPFS: https://pypi.org/project/ipfshttpclient/
