# Key-pair creation
# RSA Key
openssl genrsa -out tech-private.pem 2048
# openssl genpkey -out device.key -algorithm RSA -pkeyopt rsa_keygen_bits:2048

# ECC Key
# - https://jameshfisher.com/2017/04/14/openssl-ecc/
openssl ecparam -name secp256k1 -genkey -noout -out tech_prv_key.pem

# Public Key extraction
# RSA
openssl rsa -in tech-private.pem -outform PEM -pubout -out tech-public.pem # Chiave Pubblica

# ECC
openssl ec -in tech_priv_key.pem -pubout -out tech_pub_key.pem

